/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com3014.facebook;

import com3014.webapp.SQLConnector;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Daniel
 */
public class FacebookAuth extends HttpServlet {
    
    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        /**
         * Assumptions: User is logged in FB but has just auth app
         * The FB token stored in db is empty
         * This will take the user id from session, do sql query
         * and update with FB token
         * When complete this method will return true. If failed, will return false
         * 
         * Procedure:
         * 1. Get user id from session and fb token from post request
         * 2. Exchange the short token for a long token
         * 3. do sql update and insert key
         * 4. If no errors, respond true. Otherwise respond false + error msg
         */
        
        HttpSession session = request.getSession(false);
        String userid = (String) session.getAttribute("UserID");
        String fbuid = request.getParameter("fb_userid").toString();
        String fbtoken = request.getParameter("fb_token").toString();
        
        
        FbBean bean = new FbBean();
        fbtoken = bean.exchangeTokens(fbtoken);
        
        String sqlStatement = "UPDATE user SET FB_AccessToken='" + fbtoken + "', FB_UserId='" + fbuid + "' WHERE UserID='" + userid + "'";
        int sqlResult;
        String result = "NO";
        try {
                sqlResult = SQLConnector.performUpdate(sqlStatement);
                if (sqlResult==1) {
                    session.setAttribute("FB_UserId", fbuid);
                    session.setAttribute("FB_AccessToken", fbtoken);
                    result = "OK";
                } else if (sqlResult==0){
                    result += " - SQL error: Could not find any rows to update";
                }
               
            } catch (Exception e) {
                result += " - SQL encountered an error when updating fields:\n" + e.getMessage();
            } finally {
                SQLConnector.close();
            }
        
        response.setContentType("text/plain");
        response.setContentLength(result.length());
        PrintWriter out = response.getWriter();
        out.println(result);
        out.close();
        out.flush();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
