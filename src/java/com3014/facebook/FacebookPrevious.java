/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com3014.facebook;

import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author Carlo
 */
public class FacebookPrevious extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    HttpSession session = request.getSession(false);
        String url = (String) session.getAttribute("Fb_Previous");
//        System.out.println("url is: " + url);
        FbBean bean = new FbBean();
        //Retrieve the node from the FbBean. Will be null if there is an error
        JsonNode node = bean.homeFeedRetrival(url);
        if(node.has("paging")){
        JsonNode paging = node.get("paging");
        session.setAttribute("Fb_Previous", paging.findValue("previous").asText());
//        session.setAttribute("Fb_Next", paging.findValue("next").asText());
        }
        //Used to write the value returned in the outputstream
        response.setContentType("application/json");
        ObjectMapper mapper = new ObjectMapper();
        OutputStream out = response.getOutputStream();
        mapper.writeValue(out, node);
    }
    
    
}
