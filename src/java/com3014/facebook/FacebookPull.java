/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com3014.facebook;

import com3014.webapp.SQLConnector;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author Carlo
 */
public class FacebookPull extends HttpServlet {

    /**
     * This will be called by an Ajax call which will just retrieve the JSON object from the FbBean
     * The client side will handle manipulating the JSON object
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException 
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        String userid = (String) session.getAttribute("UserID");
//        String sqlStatement = "SELECT * FROM user WHERE UserID='" + userid + "'";
//                            session.setAttribute("FB_UserId", sqlResult.getString("FB_UserId"));
//                    session.setAttribute("FB_AccessToken", sqlResult.getString("FB_AccessToken"));
        String FBUserID = (String) session.getAttribute("FB_UserId");
        String FBToken = (String) session.getAttribute("FB_AccessToken");
//        try {
//            ResultSet results = SQLConnector.performQuery(sqlStatement);
//            // Assume there is only one result which will come back from the query
//            if(results.first()){
//                FBUserID = results.getString("FB_UserId");
//                FBToken = results.getString("FB_AccessToken");
//            }
//            // not doing an else because Facebook will return an error in json format so will have to handle errors on the client side ????????????? not sure, need to discuss
//        } catch (Exception e) {
//            // not doing an exception handle because Facebook will return an error in json format so will have to handle errors on the client side ????????????? not sure, need to discuss
//            System.out.println("FacebookPull error " + e.getMessage() + " " + e.toString());
//        } finally {
//            SQLConnector.close();
//        }
//        System.out.println("FBUserID: " + FBUserID + " and FbToken: " + FBToken);
        FbBean bean = new FbBean();
        //Retrieve the node from the FbBean. Will be null if there is an error
        JsonNode node = bean.homeFeedRetrival("https://graph.facebook.com/" + FBUserID + "/home?access_token=" + FBToken + "&limit=15");
        if(!node.has("error")){
        JsonNode paging = node.get("paging");
        session.setAttribute("Fb_Previous", paging.findValue("previous").asText());
        session.setAttribute("Fb_Next", paging.findValue("next").asText());
        }
        //Used to write the value returned in the outputstream
        response.setContentType("application/json");
        ObjectMapper mapper = new ObjectMapper();
        OutputStream out = response.getOutputStream();
        mapper.writeValue(out, node);

    }
    
    
    
    
}
