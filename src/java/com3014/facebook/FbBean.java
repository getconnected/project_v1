/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com3014.facebook;

import java.io.InputStream;
import java.util.Scanner;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author Carlo
 */
public class FbBean {
    private String baseUri= "https://graph.facebook.com";
    private String appId = "627121617301789";
    private String appSecret = "90ba73893b783078cbace2b360e7bfab";
    
    /**
     * Used to retrieve the users home feed. The limit parameter indicates how many stories comes back.
     * You can access this on a web browser as well. To  get your access token and userId use:
     * https://developers.facebook.com/tools/explorer?method=GET&path=me
     * @param userId
     * @param accessToken 
     */
    public JsonNode homeFeedRetrival(String url){
        
        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet(url);
        try{
            HttpResponse response = client.execute(get);
            int statusCode = response.getStatusLine().getStatusCode();
            HttpEntity entity = response.getEntity();
            if(entity != null){
                InputStream is = entity.getContent();
                ObjectMapper mapper = new ObjectMapper();
                JsonNode node = mapper.readTree(is);
                System.out.println(node);
                return node;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * Used to exchange the short token for a long token
     * @param accessToken
     * @return 
     */
    public String exchangeTokens(String accessToken){
        String result = "";
        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet(baseUri + "/oauth/access_token?grant_type=fb_exchange_token&client_id="
                + appId + "&client_secret=" + appSecret
                + "&fb_exchange_token=" + accessToken);
        try {
            HttpResponse response = client.execute(get);
            HttpEntity entity = response.getEntity();
            if(entity != null){
                InputStream is = entity.getContent();
                //gets the whole string from the inputstream. This is in the format accessToken=LONG_TERM_ACCESS_TOKEN&expires=TIMESTAMP
                String inputString = new Scanner(is, "UTF-8").useDelimiter("\\A").next();
                System.out.println(inputString);
                int startFrom = inputString.indexOf("&");
                result = inputString.substring(inputString.indexOf("=")+1, startFrom);
                String expires = inputString.substring(inputString.indexOf("=", startFrom) + 1);
                System.out.println(expires);
                System.out.println(result);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        
        return result;
    }
    
}
