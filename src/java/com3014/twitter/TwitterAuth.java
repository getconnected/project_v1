/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com3014.twitter;

import com3014.webapp.SQLConnector;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

/**
 *
 * @author Daniel
 */
public class TwitterAuth extends HttpServlet {


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        String userid = (String) session.getAttribute("UserID");
        Twitter twitter = (Twitter) request.getSession().getAttribute("twitter");
        RequestToken requestToken = (RequestToken) request.getSession().getAttribute("requestToken");
        String verifier = request.getParameter("oauth_verifier");
        AccessToken token = null;
        
        int sqlResult;
        String result = "NO";
        try {
            token = twitter.getOAuthAccessToken(requestToken, verifier);
            
            System.out.println(token.getToken());
            String sqlStatement = "UPDATE user SET TW_oauth_token='" + token.getToken() + "', TW_oauth_token_secret='" + token.getTokenSecret() + "' WHERE UserID='" + userid + "'";
            sqlResult = SQLConnector.performUpdate(sqlStatement);
                if (sqlResult==1) {
                    session.setAttribute("TW_oauth_token", token.getToken());
                    session.setAttribute("TW_oauth_token_secret", token.getTokenSecret());
                    result = "OK";
                    System.out.println("GETS HERE AND SETS IT");
                } else if (sqlResult==0){
                    result += " - SQL error: Could not find any rows to update";
                    System.out.println(result);
                }        
        sqlStatement = "SELECT * FROM user WHERE UserID='" + userid + "'";
        ResultSet results = SQLConnector.performQuery(sqlStatement);
            System.out.println("987987987987987");
             System.out.println(results.first());
             System.out.println(results.getString("TW_oauth_token"));
            session.removeAttribute("requestToken");
        } catch (TwitterException e) {
            throw new ServletException(e);
        } catch (Exception ex) {
            Logger.getLogger(TwitterAuth.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
                SQLConnector.close();
            }
        response.sendRedirect(request.getContextPath() + "/");
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String verified = request.getParameter("oauth_verifier");
        System.out.println("SDILFUHS;DFHS;DOIFS;OIU1!!!! " + verified);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Twitter authorisation servlet";
    }// </editor-fold>
}
