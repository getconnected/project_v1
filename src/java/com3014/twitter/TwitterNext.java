/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com3014.twitter;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import twitter4j.Paging;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.json.DataObjectFactory;

/**
 *
 * @author Carlo
 */
public class TwitterNext extends HttpServlet{
     /**
     * 
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException 
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        String TW_token = (String)session.getAttribute("TW_oauth_token");
        String TW_secret = (String)session.getAttribute("TW_oauth_token_secret");
        int page = Integer.parseInt((String)session.getAttribute("twit_page"));
        page++;

        
        ConfigurationBuilder cb = new ConfigurationBuilder();
        
        cb.setDebugEnabled(true)
            .setJSONStoreEnabled(true)
            .setOAuthConsumerKey(getServletContext().getInitParameter("consumerKey"))
            .setOAuthConsumerSecret(getServletContext().getInitParameter("consumerSecret"))
            .setOAuthAccessToken(TW_token)
            .setOAuthAccessTokenSecret(TW_secret);
        
        TwitterFactory tf = new TwitterFactory(cb.build());
        Twitter twitter = tf.getInstance();
        List<Status> statuses;
        JsonNode node;
        ObjectMapper mapper = new ObjectMapper();
        String json = "";
        try {
            Paging paging = new Paging(page, 10);
            statuses = twitter.getHomeTimeline(paging);


            json = DataObjectFactory.getRawJSON(statuses);
//            for (Status status : statuses) {
//                json += DataObjectFactory.getRawJSON(status);
//            }
//            System.out.println("json string is: " + json);
        } catch (TwitterException ex) {
            json = "{\"error\":\"unable to load twitter feed\"}";
            Logger.getLogger(TwitterGet.class.getName()).log(Level.SEVERE, null, ex);
        }
        node = mapper.readTree(json);
        response.setContentType("application/json");
        session.setAttribute("twit_page", String.valueOf(page));
        OutputStream out = response.getOutputStream();
        mapper.writeValue(out, node);
    }
}