/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com3014.twitter;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 *
 * @author Carlo
 */
public class TwitterRT extends HttpServlet
{

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        String twId = request.getParameter("twId").toString();
        String method = request.getParameter("method").toString();
        String TW_token = (String)session.getAttribute("TW_oauth_token");
        String TW_secret = (String)session.getAttribute("TW_oauth_token_secret");
        
                ConfigurationBuilder cb = new ConfigurationBuilder();
        
        cb.setDebugEnabled(true)
            .setJSONStoreEnabled(true)
            .setOAuthConsumerKey(getServletContext().getInitParameter("consumerKey"))
            .setOAuthConsumerSecret(getServletContext().getInitParameter("consumerSecret"))
            .setOAuthAccessToken(TW_token)
            .setOAuthAccessTokenSecret(TW_secret);
        
        TwitterFactory tf = new TwitterFactory(cb.build());
        Twitter twitter = tf.getInstance();
        String result = "";
        try {
            if(method.equals("destory")){
            twitter.destroyStatus(Long.parseLong(twId));
            } else {
            twitter.retweetStatus(Long.parseLong(twId));
            }
            result = "OK";
        } catch (TwitterException ex) {
            Logger.getLogger(TwitterRT.class.getName()).log(Level.SEVERE, null, ex);
            result = "Error";
        }
        
        response.setContentType("text/plain");
        response.setContentLength(result.length());
        PrintWriter out = response.getWriter();
        out.println(result);
        out.close();
        out.flush();
    }

    
    
}
