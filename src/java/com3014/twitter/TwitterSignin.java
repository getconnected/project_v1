/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com3014.twitter;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

/**
 *
 * @author Daryl
 */
public class TwitterSignin extends HttpServlet{
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * Code adapted from Twitter4j tutorial 
     * from http://www.codehandling.com/2012/11/twitter-log-in-on-your-website-with.html
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        String userid = (String) session.getAttribute("UserID");
        
        try {
            StringBuffer callback = request.getRequestURL();
            int index = callback.lastIndexOf("/");
            callback.replace(index, callback.length(), "").append("/TwitterAuth");
            
            System.out.println("CALLBSFSD:PSDJVD" + callback.toString());
            ConfigurationBuilder config = new ConfigurationBuilder();
            config.setOAuthConsumerKey(getServletContext().getInitParameter("consumerKey"));
            config.setOAuthConsumerSecret(getServletContext().getInitParameter("consumerSecret"));
            System.out.println(getServletContext().getInitParameter("consumerKey"));
            System.out.println(getServletContext().getInitParameter("consumerSecret"));
            TwitterFactory twitFact = new TwitterFactory(config.build());
            Twitter twitter = twitFact.getInstance();
            request.getSession().setAttribute("twitter", twitter);
            
            RequestToken requestToken = twitter.getOAuthRequestToken(callback.toString());
            request.getSession().setAttribute("requestToken", requestToken);
            System.out.println(requestToken.getAuthenticationURL());
            response.sendRedirect(requestToken.getAuthenticationURL());

        } catch (TwitterException e) {
            throw new ServletException(e);
        }

    }
    
}
