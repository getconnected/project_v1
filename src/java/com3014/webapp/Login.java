/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com3014.webapp;

import static com3014.webapp.Register.DASHBOARD_URL;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.binary.Hex;

/**
 *
 * @author Andy Smith - as00288 The servlet which processes the Sign Up form,
 * redirecting to dashboard upon success
 */
@WebServlet("/Login")
public class Login extends HttpServlet {

    /* The relative URL of the main dashboard page */
    public static final String DASHBOARD_URL = "dashboard.jsp";

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Don't do anything. Shouldn't come here by GET.        
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Setup a way to print to the page
        PrintWriter out = response.getWriter();

        // We are going to send a plain message back
        response.setContentType("text/plain");
        
        // Initialise a load of variables for the posted data
        String username = "";
        String password = "";
        String dataOnly = "";

        // Try and get all the values
        try {
            username = request.getParameter("username").toString();
            password = request.getParameter("password").toString();
            dataOnly = request.getParameter("dataOnly").toString();
        } catch (Exception e) {
            // We check for blank below so we don't mind if this errored
        }

        // Direct on unless we've come via AJAX so only want a resposne
        boolean redirect = (!dataOnly.equals("Y"));

        String error = "";

        if (username.equals("") || password.equals("")) {
            error = "You left a field blank!";
        } else {
            // Hash the password with SHA256
            MessageDigest digest;
            String encPassword = "";
            try {
                digest = MessageDigest.getInstance("SHA-256");
                encPassword = Hex.encodeHexString(digest.digest(password.getBytes("UTF-8")));
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                error = "SERVER ERROR: Encryption failed";
                out.println(error);
            }

            String sqlStatement = "SELECT * FROM user WHERE UserID = '" + username + "' AND Password = '" + encPassword + "'";
            ResultSet sqlResult;

            try {
                sqlResult = SQLConnector.performQuery(sqlStatement);
                if (!sqlResult.next()) {
                    error = "Incorrect username and/or password";
                } else {
                    HttpSession session = request.getSession(true);
                    session.setAttribute("UserID", sqlResult.getString("UserID"));
                    session.setAttribute("FirstName", sqlResult.getString("FirstName"));
                    session.setAttribute("Surname", sqlResult.getString("Surname"));
                    session.setAttribute("FB_UserId", sqlResult.getString("FB_UserId"));
                    session.setAttribute("FB_AccessToken", sqlResult.getString("FB_AccessToken"));
                    session.setAttribute("TW_oauth_token", sqlResult.getString("TW_oauth_token"));
                    session.setAttribute("TW_oauth_token_secret", sqlResult.getString("TW_oauth_token_secret"));
                }
            } catch (Exception e) {
                error = "Incorrect username and/or password";
            } finally {
                SQLConnector.close();
            }
        }

        if (error.equals("")) {
            // Result was found
            if (redirect) {
                request.getRequestDispatcher(DASHBOARD_URL).forward(request, response);
            } else {
                out.println("SUCCESS:" + DASHBOARD_URL);
            }
        } else {
            if (redirect) {
                request.getRequestDispatcher("index.jsp").forward(request, response);
            } else {
                if (error.equals("")) {
                    error = "Incorrect username and/or password";
                }
                out.println(error);
            }
        }
    }
}