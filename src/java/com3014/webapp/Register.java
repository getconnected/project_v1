/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com3014.webapp;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.binary.Hex;

/**
 *
 * @author Andy Smith - as00288 The servlet which processes the Sign Up form,
 * redirecting to dashboard upon success
 */
@WebServlet("/Register")
public class Register extends HttpServlet {

    /* The relative URL of the main dashboard page */
    public static final String DASHBOARD_URL = "dashboard.jsp";

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Don't do anything. Shouldn't come here by GET.        
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Setup a way to print to the page
        PrintWriter out = response.getWriter();

        // We are going to send a plain message back
        response.setContentType("text/plain");

        // Initialise a load of variables for the posted data
        String firstName = "";
        String surname = "";
        String username = "";
        String password = "";
        String confirm = "";
        String dataOnly = "";

        // Try and get all the values
        try {
            firstName = request.getParameter("firstName").toString();
            surname = request.getParameter("surname").toString();
            username = request.getParameter("username").toString();
            password = request.getParameter("password").toString();
            confirm = request.getParameter("confirm").toString();
            dataOnly = request.getParameter("dataOnly").toString();
        } catch (Exception e) {
            // We check for blank below so we don't mind if this errored
        }

        // Direct on unless we've come via AJAX so only want a resposne
        boolean redirect = (!dataOnly.equals("Y"));

        String error = "";

        if (firstName.equals("") || surname.equals("") || username.equals("") || password.equals("") || confirm.equals("")) {
            error = "You left a field blank!";
        } else if (!password.equals(confirm)) {
            error = "The passwords did not match";
        } else {
            // Hash the password with SHA256
            MessageDigest digest;
            String encPassword = "";
            try {
                digest = MessageDigest.getInstance("SHA-256");
                encPassword = Hex.encodeHexString(digest.digest(password.getBytes("UTF-8")));
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(Register.class.getName()).log(Level.SEVERE, null, ex);
                error = "SERVER ERROR: Encryption failed";
                out.println(error);
            }


            String sqlStatement = "INSERT INTO user(UserID, FirstName, Surname, Password) VALUES ('" + username + "', '" + firstName + "', '" + surname + "', '" + encPassword + "')";
            int sqlResult = 0;

            try {
                sqlResult = SQLConnector.performUpdate(sqlStatement);
                HttpSession session = request.getSession(true);
                session.setAttribute("UserID", username);
                session.setAttribute("FirstName", firstName);
                session.setAttribute("Surname", surname);
            } catch (Exception e) {
                System.out.println(e.getStackTrace());
                error = "SERVER ERROR - Performing SQL update or creating session";
            } finally {
                SQLConnector.close();
            }

            if (sqlResult == 0) {
                // The user already exists! Woops!
                error = "An account with that username already exists!";
            }
        }

        if (error.equals("")) {
            // It all went okay! New user has been created
            if (redirect) {
                request.getRequestDispatcher(DASHBOARD_URL).forward(request, response);
            } else {
                out.println("SUCCESS:" + DASHBOARD_URL);
            }
        } else {
            // There was an error!
            if (redirect) {
                request.getRequestDispatcher("index.jsp").forward(request, response);
            } else {
                out.println(error);
            }
        }
    }
}