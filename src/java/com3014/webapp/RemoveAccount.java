/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com3014.webapp;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Daniel He - 
 */
@WebServlet("/RemoveAccount")
public class RemoveAccount extends HttpServlet {


    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Don't do anything. Shouldn't come here by GET.        
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        HttpSession session = request.getSession(false);
        String userid = (String) session.getAttribute("UserID");
        System.out.println(userid);
                   String sqlStatement = "DELETE FROM user WHERE UserID='" + userid + "'";

        try {
                SQLConnector.performUpdate(sqlStatement);
               
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                SQLConnector.close();
                request.getSession().invalidate();
        response.sendRedirect("index.htm");
            }
    }
}