/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com3014.webapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Andy
 */
public class SQLConnector {

    /**
    private final static String DATABASE_USERNAME = "b5435dbad289d6";
    private final static String DATABASE_PASSWORD = "ae21f2bf";
    private final static String DATABASE_SERVER = "us-cdbr-east-03.cleardb.com";
    private final static String DATABASE_SCHEMA = "heroku_03841ddd3170471";
    */
    private final static String DATABASE_USERNAME = "daniel";
    private final static String DATABASE_PASSWORD = "password";
    private final static String DATABASE_SERVER = "getconnected.ceap65zbzvkl.us-west-2.rds.amazonaws.com";
    private final static String DATABASE_SCHEMA = "get_conn";
    
    private static Connection connect;
    private static Statement statement = null;
    private static ResultSet resultSet = null;

    public static int performUpdate(String updateStatement) throws Exception {
        int resultStatus = 0;
        resultStatus = getConnection().executeUpdate(updateStatement);
        return resultStatus;
    }

    public static ResultSet performQuery(String queryStatement) throws Exception {
        resultSet = getConnection().executeQuery(queryStatement);
        return resultSet;
    }

    private static Statement getConnection() {
        statement = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection("jdbc:mysql://" + DATABASE_SERVER + "/" + DATABASE_SCHEMA + "?"
                    + "user=" + DATABASE_USERNAME + "&password=" + DATABASE_PASSWORD);
            statement = connect.createStatement();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return statement;
    }

    public static void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {
        }
    }
}