/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com3014.webapp;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Daniel
 */
public class UserConnStatus extends HttpServlet {



    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("called");
        HttpSession session = request.getSession(false);
        String connectionsAvailable = "YES";
                    
                    Object fb_userid = session.getAttribute("FB_UserId");
                    Object tw_token = session.getAttribute("TW_oauth_token");
                    if (fb_userid==null) {
                        fb_userid = "";
                    }
                    if (tw_token==null) {
                        tw_token = "";
                    }
                    if (fb_userid.toString().isEmpty() && tw_token.toString().isEmpty()) {
                        connectionsAvailable = "NO";
                    } else if (!fb_userid.toString().isEmpty() && tw_token.toString().isEmpty()){
                        connectionsAvailable = "FB" + (String)fb_userid;
                    } else if (fb_userid.toString().isEmpty() && !tw_token.toString().isEmpty()){
                        connectionsAvailable = "TW" + (String)tw_token;
                    } else{
                        connectionsAvailable = (String)fb_userid + "##" + (String)tw_token;
                    } 
                    System.out.println(connectionsAvailable);
                    response.setContentType("text/plain");
        response.setContentLength(connectionsAvailable.length());
        PrintWriter out = response.getWriter();
        out.println(connectionsAvailable);
        out.close();
        out.flush();
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
