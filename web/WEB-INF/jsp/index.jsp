<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/styles.css"/>
        <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="js/scripts.js" type="text/javascript"></script>
        <title>Get Connected!</title>
    </head>

    <body>
        <div class="logincontainer">
            <div class="loginlogo"></div>
        </div>

        <div class="loginbox">
            <form class='loginform' onsubmit="return submitLogin()">
                <table border='0' cellpadding='5px' class='logintable'>
                    <tr><td colspan='2'><h3>Login</h3></td></tr>
                    <tr><td>Username:</td><td><input type="text" id="txtUsername" name="txtUsername" placeholder="Username or email address" size="30" required></td></tr>
                    <tr><td>Password</td><td><input type="password" id="pwdPassword" name="pwdPassword" placeholder="Password" size="30" required></td></tr>
                    <tr><td>&nbsp;</td><td><input type="submit" value="Log In"></td></tr>
                    <tr class='smalltext'><td>&nbsp;</td><td><div onclick='signup()'>Sign up now!</div></td></tr>
                </table>
            </form> 
        </div>
    </body>
</html>
