<%-- 
    Document   : dashboard
    Created on : 16-Apr-2013, 20:08:54
    Author     : Daniel
--%>
<%
    if (request.getSession(false).getAttribute("UserID") == null) {
        response.sendRedirect("index.htm");
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/dash.css"/>
        <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui.js" type="text/javascript"></script>
        <script src="js/jquery.timer.js" type="text/javascript"></script>
        <script src="js/script-dashboard.js" type="text/javascript"></script>
        <title>Dashboard</title>
    </head>

    <body>

        <div id="fb-root"></div>

        <script>   
            window.fbAsyncInit = function() {
    // init the FB JS SDK
    FB.init({
      appId      : '627121617301789',                        // App ID from the app dashboard
      channelUrl : 'http://localhost:8084/Web_v1/channel.html', // Channel file for x-domain comms
      status     : true,                                 // Check Facebook Login status
      xfbml      : true,
      oauth: true
    });
  };
             (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/all.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
   

   

</script>

<div class="headerbar"><img src="img/logo-text.png" alt="Logo"/>
    <div class="buttons">
        <div class='link' onclick="refresh()"> Refresh</div> <div class='link' onclick="expandSettings()">Settings </div> <div class='linkright' onclick="logout()">Log Out</div>
    </div>
</div>

        <div id='container' >

            <div class="loader"><img src="img/ajax-loader.gif" alt="Loading animation"/><br/>Your data is currently being loaded...</div>
        </div>
        <div class="slide" onclick="expandSettings()"> 
            <div class="slide-text"></div>
        </div>


        <!-- Hidden overlay that is only displayed when no FB/TW tokens detected -->
        <div class="networklink">

            <div class="networklinkheader">Let's Get Connected!
                <div class="closenetworklink" onclick="closeconnbox()">Close X</div>
            </div>

            <div class="networklinkcontent" id="networklinkcontenttop">
                It looks like your first time here on GetConnected. To begin using some of the features, please link a social network account below:
            </div>

            <!--TODO Needs changing. Originally used special jQuery tabs which required ul/li but now not needed, change back to divs?-->
            <div class="socialnetworktabs">
                <ul class='tabs'>
                    <li onclick="FB_login();"><a href='#fblogin'><img id="fbloginimg" src="img/facebooklogo.png" alt="Facebook" onclick="FB_login();"/></a></li>
                    <li><a href='TwitterSignin'><img src="img/twitterlogo.png" alt="Twitter"/></a></li>
                </ul>
            </div>

            <div class="networklinkcontent">
                When you've finished, click the close button on the top right hand corner to begin!
            </div>
        </div>
           <!-- Main div content here. Use jQuery to populate with FB/TW data using entry divs -->

        <div class="footer">Application created by the GetConnected team</div>
    </body>
</html>
