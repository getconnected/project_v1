/**********************************************************************
 Client-side JavaScript for dashboard
 **********************************************************************/

//------------Variables------------------------------------
var settingsON = false; //Whether the settings has been opened
var canDoFb = true;
var canDoRefresh = true;
var timer = $.timer(refresh, 5000, false);
var allids;
var facebookid;
var receiveddata;
var fbLogin = false;
var twLogin = false;
var canDoTw = true;
/**
 * Called when the dashboard loads. Does a GET request to see if any tokens are
 * available for the user. If not, the overlay is shown. Otherwise the dashboard
 * loads as normal and begins populating.
 */
$(document).ready(function() {

    //Do a check for FB/TW api key. If both ==null, then do the following:
    $.get('UserConnStatus', function(data) {
        receiveddata = data;
        //If no connections, display overlay
        if (data == "NO") {
            $("html").css("background-color", "#BDBDBD");
            $("body").css("background-color", "#BDBDBD");
            $(".slide").css("display", "none");
            $(".content").css("display", "none");
            $(".loader").css("display", "none");
            $(".networklink").css("display", "block");
            $(".networklink").css("z-index", "99999999999999999999");
            $("#container").css("z-index", "-99999999999999999999");
        }
        //Maybe conditions for just FB/TW and both?
        else {
            console.log("data: " + data);
            //If both tw and fb ids are available
            if (data.indexOf("##") !== -1) {
                twLogin = true;
                fbLogin = true;
                facebookid = data.split("##")[0];
                $("#container").html("<div class='loader'><img src='img/ajax-loader.gif'/><br/>Your data is currently being loaded...</div>")
                populateFacebookData('FacebookPull');
                populateTwitterData('TwitterGet');
            } else if (data.indexOf("FB") !== -1) {
                fbLogin = true;
                facebookid = data.substring(2);
                $("#container").html("<div class='loader'><img src='img/ajax-loader.gif'/><br/>Your data is currently being loaded...</div>")
                populateFacebookData('FacebookPull');
            } //Maybe add another check for twitter token? But we probably don't need it here
            else {
                twLogin = true;
                $("#container").html("<div class='loader'><img src='img/ajax-loader.gif'/><br/>Your data is currently being loaded...</div>")
                populateTwitterData('TwitterGet');
            }
        }
        $("#container").scroll(scrolled);
    });


});

var optionbox = "<div id='options' class='options'><div class='buttonlike'><img src='img/like.png'/></div><div class='buttoncomment'><img src='img/comment.png'/></div><div class='buttonretweet'><img src='img/tweet.png''/></div></div>";

function showButtons(a) {
    var container = document.getElementById(a);
    var message = "The width of the contents with padding: " + container.scrollWidth + "px.\n";
    message += "The height of the contents with padding: " + container.scrollHeight + "px.\n";


    //If can be expanded
    if (container.scrollHeight > 250) {
        var expanded = $("#" + a).height();
        var increase = (container.scrollHeight - 250) + 50;
        var decrease = (expanded - 250);
        $("#" + a).animate({
            height: (expanded == 250) ? "+=" + increase + "px" : (expanded > 200) ? "-=" + decrease + "px" : ""
        }, 500,
                //method when complete
                        function() {

                        });

            }


    $(".options_" + a).fadeToggle("slow");

}


var openedCommentBoxId = 0;

function showCommentBox(id) {

    if (openedCommentBoxId != 0) {
        if (openedCommentBoxId == id) {
            hideCommentBox(id);
            openedCommentBoxId = 0;
            return;
        }

    }
    openedCommentBoxId = id;
    $("#" + id).append("<span id='commentsection_" + id + "'><br/>&nbsp;<input type='textarea' onkeydown='if (event.keyCode == 13) { postComment(\"" + id + "\", this.value);hideCommentBox(\"" + id + "\");return false;}' style='font-size:16px' id='comment_" + id + "' placeholder='Type a comment and hit ENTER' size='28'></span>")

    $("#comment_" + id).click(function(event) {
        event.stopPropagation();
    });
    var container = document.getElementById(id);
    if (container.scrollHeight > 250) {
        $("#" + id).animate({
            height: ("+=60px")
        }, 500,
                //method when complete
                        function() {
                        });
            }
    $(".options_" + id).click(function(event) {
        event.stopPropagation();
    });
}

function postComment(fullid, msg) {

    FB.api("/" + fullid + "/comments", "post", {message: msg}, function(response) {

        $("#" + fullid).effect("highlight",
                {color: "#BDD9BF"}, 1000);
    });
}

function hideCommentBox(id) {

    $("#commentsection_" + id).remove();
    var curheight = $("#" + id).height();

    if (curheight > 250) {
        var decrease = curheight - 250;
        $("#" + id).animate({height: "-=" + decrease + "px"}, 500, function() {
        });
    }
    $(".options_" + id).fadeToggle("slow");

}

var showfbselected = true;
var showtwselected = true;
/**
 *Expands the settings bar (called from onClick)
 */
function expandSettings() {

    if (settingsON) {
        return;
    }
    $("#container").css("z-index", "-9999999");
    $(".slide").css("z-index", "+9999999");
    $(".slide-text").html("");

    $(".slide").animate({
        width: ("+=350px")
    }, 500,
            //method when complete
                    function() {


                        //Turn on
                        settingsON = true;
                        $(".slide-text").html("<h1>Settings</h1><br/></br>\n\
    <table class='settingstable' >\n\
<tr><td>Facebook posts:</td><td><div class='hidefb' onclick='hidefb()'>Hide</div></td><td><div class='showfb' onclick='showfb()'>Show</div></td></tr>\n\
<tr><td colspan='3'>&nbsp;</td></tr>\n\
<tr><td>Twitter posts:</td><td><div class='hidetw' onclick='hidetw()'>Hide</div></td><td><div class='showtw' onclick='showtw()'>Show</div></td></tr>\n\
<tr><td colspan='3'>&nbsp;</td></tr>\n\
<tr><td colspan='3'>&nbsp;</td></tr>\n\
<tr><td colspan='3'>&nbsp;</td></tr>\n\
<tr><td colspan='3'><div onclick='deleteAcc()' class='settingshover'>Delete account</div></td></tr>\n\
<tr><td colspan='3'>&nbsp;</td></tr>\n\
<tr><td colspan='3'><div onclick='closeSettings()' class='settingshover'>Close settings</div></td></tr>\n\
</table><br/><br/><br/><br/>");
                        if (showfbselected) {
                            showfb();
                        } else {
                            hidefb();
                        }

                        if (showtwselected) {
                            showtw();
                        } else {
                            hidetw();
                        }
                        $(".slide").click(function(event) {
                            event.stopPropagation();
                        });
                    });
        }



function popupSettings(open) {
    if (!settingsON) {


        if (open) {

            $(".slide").animate({
                width: ("-=20px")
            }, 300, function() {
                $("#container").css("z-index", "9999999");
                $(".slide").css("z-index", "-9999999");
            });
        } else {
            $("#container").css("z-index", "-9999999");
            $(".slide").css("z-index", "+9999999");
            $(".slide").animate({
                width: ("+=20px")
            }, 300);
        }
    }
}

function hidefb() {
    $(".entryfb").fadeOut();
    $(".hidefb").css("text-decoration", "underline");
    $(".showfb").css("text-decoration", "none");
    showfbselected = false;
}

function showfb() {
    $(".entryfb").fadeIn();
    $(".hidefb").css("text-decoration", "none");
    $(".showfb").css("text-decoration", "underline");
    showfbselected = true;
}

function hidetw() {
    $(".entrytw").fadeOut();
    $(".hidetw").css("text-decoration", "underline");
    $(".showtw").css("text-decoration", "none");
    showtwselected = false;
}

function showtw() {
    $(".entrytw").fadeIn();
    $(".hidetw").css("text-decoration", "none");
    $(".showtw").css("text-decoration", "underline");
    showtwselected = true;
}

function closeSettings() {
    $("#container").css("z-index", "-9999999");
    $(".slide").css("z-index", "+9999999");
    $(".slide-text").html("");

    $(".slide").animate({
        width: ("-=350px")
    }, 500,
            //method when complete
                    function() {

                        //Turn off

                        $("#container").removeAttr('style');
                        $(".slide").removeAttr('style');
                        settingsON = false;

                    });
        }

/**
 * Populates the container div with facebook data
 * @param {String} method Either FacebookPull for first time, FacebookNext for last page of data or FacebookPrevious for first page of data
 */
function populateFacebookData(method) {
    $(".content").append("<div class='entry'>Data</div>");
    jQuery.support.cors = true;
    var time = method + "?timestamp=" + new Date().getTime();
    $.getJSON(time, function(result) {
        if (result.hasOwnProperty('error')) {
            console.log("Facebook errors! " + result.error.message);
            $("html").css("background-color", "#BDBDBD");
            $("body").css("background-color", "#BDBDBD");
            $(".slide").css("display", "none");
            $(".content").css("display", "none");
            $(".loader").css("display", "none");
            $(".divloader").css("display", "none");
            $("#networklinkcontenttop").html("Opps! There has been an error on Facebook. Please log in again");
            $(".networklink").css("display", "block");
            $(".networklink").css("z-index", "99999999999999999999");
            //$("#container").html("");
            $("#container").css("z-index", "-99999999999999999999");
        } else {
            $.each(result.data, function(index, item) {
                var userLikedPost = false;

                //Get likes information
                for (i in item.likes) {
                    for (j in item.likes[i]) {
                        if (item.likes[i][j].id === facebookid) {
                            userLikedPost = true;
                            break;
                        }
                    }
                }

                //Different types of posts which come up in a news feed
                if (!$("#" + item.id).length) {
                    switch (item.type)
                    {
                        case "link":
                            if (item.hasOwnProperty('message')) {
                                if (item.hasOwnProperty('to')) {
                                    createFbLinkToAdd(item.from.id, item.to.data[0].id, item.from.name, item.to.data[0].name, item.message, item.link, item.id, item.name, item.created_time, userLikedPost);
                                } else {
                                    createFbLinkAdd(item.from.id, item.from.name, item.message, item.link, item.id, item.name, item.created_time, userLikedPost);
                                }
                            } else if (item.hasOwnProperty('story')) {
                                if (item.hasOwnProperty('to')) {
                                    createFbLinkToAdd(item.from.id, item.to.data[0].id, item.from.name, item.to.data[0].name, item.story, item.link, item.id, item.name, item.created_time, userLikedPost);
                                } else {
                                    createFbLinkAdd(item.from.id, item.from.name, item.story, item.link, item.id, item.name, item.created_time, userLikedPost);
                                }
                            } else {
                                if (item.hasOwnProperty('to')) {
                                    createFbLinkToAdd(item.from.id, item.to.data[0].id, item.from.name, item.to.data[0].name, "added a link", item.link, item.id, item.name, item.created_time, userLikedPost);
                                } else {
                                    createFbLinkAdd(item.from.id, item.from.name, "added a link", item.link, item.id, item.name, item.created_time, userLikedPost);
                                }
                            }
                            break;
                        case "status":
                            /**
                             * found that wall post appeared in the type status
                             * status_type was a truer representation in this case
                             * Couldn't find the case when a status is uploaded
                             * from a computer, always just had mobile
                             */
                            switch (item.status_type) {
                                case "mobile_status_update":

                                    /**
                                     * This also handles checkins. For some reason it comes up
                                     * as a status. If there is no message then it comes up as
                                     * a story key& value. Example is
                                     *  *User's name* is at *location*
                                     */
                                    if (item.hasOwnProperty('message')) {
                                        createFbStatusElement(item.from.id, item.from.name, item.message, item.id, item.created_time, userLikedPost);
                                    } else if (item.hasOwnProperty('story')) {
                                        createFbStatusElement(item.from.id, item.from.name, item.story, item.id, item.created_time, userLikedPost);
                                    } else {
                                        createFbStatusElement(item.from.id, item.from.name, "added a status", item.id, item.created_time, userLikedPost);
                                    }
                                    break;
                                case "wall_post":

                                    createFbWallPost(item.from.id, item.to.data[0].id, item.from.name, item.to.data[0].name, item.message, item.id, item.created_time, userLikedPost);
                                    break;
                            }

                            break;
                        case "photo":

                            /**
                             * If someone has uploaded a photo without a message
                             * then it comes up as a story. For example
                             *  *User's name* has updated their cover photo
                             */
                            if (item.hasOwnProperty('message')) {
                                if (item.hasOwnProperty('to')) {
                                    createFbMediaToAdd(item.from.id, item.to.data[0].id, item.from.name, item.to.data[0].name, item.message, item.picture, item.link, item.id, item.created_time, "posted a photo to", userLikedPost);
                                } else {
                                    createFbMediaAdd(item.from.id, item.from.name, item.message, item.picture, item.link, item.id, item.created_time, "posted a photo", userLikedPost);
                                }
                            } else if (item.hasOwnProperty('story')) {
                                if (item.hasOwnProperty('to')) {
                                    createFbMediaToAdd(item.from.id, item.to.data[0].id, item.from.name, item.to.data[0].name, item.story, item.picture, item.link, item.id, item.created_time, "posted a photo to", userLikedPost);
                                } else {
                                    createFbMediaAdd(item.from.id, item.from.name, item.story, item.picture, item.link, item.id, item.created_time, "posted a photo", userLikedPost);
                                }
                            } else {
                                if (item.hasOwnProperty('to')) {
                                    createFbMediaToAdd(item.from.id, item.to.data[0].id, item.from.name, item.to.data[0].name, "added a photo", item.picture, item.link, item.id, item.created_time, "posted a photo to", userLikedPost);
                                } else {
                                    createFbMediaAdd(item.from.id, item.from.name, "added a photo", item.picture, item.link, item.id, item.created_time, "posted a photo", userLikedPost);
                                }
                            }
                            break;
                        case "video":
                            if (item.hasOwnProperty('message')) {
                                if (item.hasOwnProperty('to')) {
                                    createFbMediaToAdd(item.from.id, item.to.data[0].id, item.from.name, item.to.data[0].name, item.message, item.picture, item.link, item.id, item.created_time, "posted a video to", userLikedPost);
                                } else {
                                    createFbMediaAdd(item.from.id, item.from.name, item.message, item.picture, item.link, item.id, item.created_time, "posted a video", userLikedPost);
                                }
                            } else if (item.hasOwnProperty('story')) {
                                if (item.hasOwnProperty('to')) {
                                    createFbMediaToAdd(item.from.id, item.to.data[0].id, item.from.name, item.to.data[0].name, item.story, item.picture, item.link, item.id, item.created_time, "posted a video to", userLikedPost);
                                } else {
                                    createFbMediaAdd(item.from.id, item.from.name, item.story, item.picture, item.link, item.id, item.created_time, "posted a video", userLikedPost);
                                }
                            } else {
                                if (item.hasOwnProperty('to')) {
                                    createFbMediaToAdd(item.from.id, item.to.data[0].id, item.from.name, item.to.data[0].name, "added a video", item.picture, item.link, item.id, item.created_time, "posted a video to", userLikedPost);
                                } else {
                                    createFbMediaAdd(item.from.id, item.from.name, "added a video", item.picture, item.link, item.id, item.created_time, "posted a video", userLikedPost);
                                }
                            }
                            break;

                    }
                }
            });
            if (result.data.length > 0) {
                $('#container .entry').sort(CustomSort).prependTo('#container');
            }
            $(".loader").css("display", "none");
            $(".divloader").css("display", "none");
            if (method === 'FacebookPull') {
                timer.play();
                canDoFb = true;
                canDoRefresh = true;
            } else {
                canDoFb = true;
                canDoRefresh = true;
            }
        }

    });
}

function populateTwitterData(method) {
    $(".content").append("<div class='entry'>Data</div>");
    jQuery.support.cors = true;
    var time = method + "?timestamp=" + new Date().getTime();
    $.getJSON(time, function(result) {
        console.log(result);
        if (result.hasOwnProperty('error')) {
            console.log("Twitter error! " + result.error.message);
            $("html").css("background-color", "#BDBDBD");
            $("body").css("background-color", "#BDBDBD");
            $(".slide").css("display", "none");
            $(".content").css("display", "none");
            $(".loader").css("display", "none");
            $(".divloader").css("display", "none");
            //$("#container").html("");
            $("#container").css("z-index", "-99999999999999999999");
            $("#networklinkcontenttop").html("Opps! There has been an error on Twitter. Please log in again");
            $(".networklink").css("display", "block");
        } else {
            $.each(result, function(index, item) {

                if (!$("#" + item.id).length) {
                    createTweetElement(item.id, item.user.screen_name, item.user.profile_image_url, item.user.name, item.text, item.created_at, item.retweeted, item.favorited);
                }
            });

            $(".loader").css("display", "none");
            $(".divloader").css("display", "none");
            $('#container .entry').sort(CustomSort).prependTo('#container');
            if (method === 'TwitterGet') {
                timer.play();
                canDoTw = true;
                canDoRefresh = true;
            } else {
                canDoTw = true;
                canDoRefresh = true;
            }
        }
    });
}

function CustomSort(a, b)
{
    var dateA = new Date(a.date);
    if (isNaN(dateA)) {
        myDate = new Date(Date.parse(a.date.replace(/\-/ig, '/').split('.')[0]));
    }
    var dateB = new Date(b.date);
    if (isNaN(dateB)) {
        myDate = new Date(Date.parse(b.date.replace(/\-/ig, '/').split('.')[0]));
    }

    if (dateA - dateB <= 0) {
        return 1;
    } else {
        return -1;
    }

}

function previous() {
    if (canDoFb) {
        canDoFb = false;
        $(".loader").css("display", "block");
        populateFacebookData('FacebookPrevious');
    }
}

function refresh() {
    timer.reset();
    if (canDoRefresh) {
        canDoRefresh = false;
        populateFacebookData('FacebookPrevious');
    }
}

function scrolled() {
    if ($("#container").prop('scrollHeight') - $("#container").scrollTop() <= $("#container").height() && canDoFb && canDoTw) {

        $("<div class='divloader'><br/><br/>Loading more posts...<br/><br/><img src='img/ajax-loader-scroll.gif'></div>").appendTo("#container");

        if (twLogin && fbLogin) {
            canDoFb = false;
            canDoTw = false;
            populateFacebookData('FacebookPull');
            populateTwitterData('TwitterNext');
        } else if (fbLogin && !twLogin) {
            canDoFb = false;
            populateFacebookData('FacebookPull');
        } //Maybe add another check for twitter token? But we probably don't need it here
        else if (!fbLogin && twLogin) {
            canDoTw = false;
            populateTwitterData('TwitterNext');
        }
    }
}

var options = {
    autoResize: true, // This will auto-update the layout when the browser window is resized.
    container: $('.content'), // Optional, used for some extra CSS styling
    offset: 3 // Optional, the distance between grid items

};

function createFbLinkToAdd(fromId, toId, from, to, message, link, msgId, name, date, userLikedPost) {
    $("<div id='" + msgId + "' onclick=\"showButtons('" + msgId + "')\" class='entryfb entry' data='" + date + "'>\n\
            <div class='head'>\n\
                <div>\n\
                    <a href='http://www.facebook.com/" + fromId + "' target='_blank'>\n\
                        <img class='fbimg' alt='Profile Image' src='https://graph.facebook.com/" + fromId + "/picture?type=square'/>\n\
                    </a>\n\
                </div>\n\
                <div class='headCont'>\n\
                    <h4 id='name'>\n\
                        <a href='http://www.facebook.com/" + fromId + "' target='_blank'>\n\
                            " + from + "         \n\
                        </a>\n\
                    posted a link to \n\
                    <a href='http://www.facebook.com/" + toId + "' target='_blank'>\n\
                            " + to + "         \n\
                        </a>\n\
                    </h4>\n\
                </div>                \n\
            </div>            \n\
            <div class='message'>\n\
                " + message + "            \n\
            </div>\n\
            <a class='fbMsgLink' href='" + link + "' target='_blank'>\n\
                    " + name + "\n\
                </a>\n\
            <div id='options' class='options_" + msgId + "'>\n\
                <div onclick='likeComment(\"" + msgId + "\")' id='buttonlike_" + msgId + "' class='" + (userLikedPost ? "buttonliked" : "buttonunlike") + "'>\n\
                    <img id='like_" + msgId + "' src='img/" + (userLikedPost ? "likeok.png" : "like.png") + "'/>\n\
                </div>\n\
                <div class='buttoncomment' onclick='showCommentBox(\"" + msgId + "\")' id='buttoncomment_" + msgId + "'>\n\
                    <img src='img/comment.png'/>\n\
                </div>\n\
            </div>").hide().appendTo("#container").fadeIn('slow'); //.masonry( 'reload' );
    document.getElementById(msgId).date = date;
}

function createFbLinkAdd(fromId, from, message, link, msgId, name, date, userLikedPost) {
    $("<div id='" + msgId + "' onclick=\"showButtons('" + msgId + "')\" class='entryfb entry' data='" + date + "'>\n\
            <div class='head'>\n\
                <div>\n\
                    <a href='http://www.facebook.com/" + fromId + "' target='_blank'>\n\
                        <img class='fbimg' alt='Profile Image' src='https://graph.facebook.com/" + fromId + "/picture?type=square'/>\n\
                    </a>\n\
                </div>\n\
                <div class='headCont'>\n\
                    <h4 id='name'>\n\
                        <a href='http://www.facebook.com/" + fromId + "' target='_blank'>\n\
                            " + from + "         \n\
                        </a>\n\
                    posted a link</h4>\n\
                </div>                \n\
            </div>            \n\
            <div class='message'>\n\
                " + message + "            \n\
            </div>\n\
            <a class='fbMsgLink' href='" + link + "' target='_blank'>\n\
                    " + (typeof name === 'undefined' ? 'Link' : name) + "\n\
                </a>\n\
            <div id='options' class='options_" + msgId + "'>\n\
                <div onclick='likeComment(\"" + msgId + "\")' id='buttonlike_" + msgId + "' class='" + (userLikedPost ? "buttonliked" : "buttonunlike") + "'>\n\
                    <img id='like_" + msgId + "' src='img/" + (userLikedPost ? "likeok.png" : "like.png") + "'/>\n\
                </div>\n\
                <div class='buttoncomment' onclick='showCommentBox(\"" + msgId + "\")' id='buttoncomment_" + msgId + "'>\n\
                    <img src='img/comment.png'/>\n\
                </div>\n\
            </div>").hide().appendTo("#container").fadeIn('slow'); //.masonry( 'reload' );
    document.getElementById(msgId).date = date;
}

function createFbMediaToAdd(fromId, toId, from, to, message, imgSrc, imgLink, msgId, date, type, userLikedPost)
{
    $("<div id='" + msgId + "' onclick=\"showButtons('" + msgId + "')\" class='entryfb entry' data='" + date + "'>\n\
            <div class='head'>\n\
                <div>\n\
                    <a href='http://www.facebook.com/" + fromId + "' target='_blank'>\n\
                        <img class='fbimg' alt='Profile Image' src='https://graph.facebook.com/" + fromId + "/picture?type=square'/>\n\
                    </a>\n\
                </div>\n\
                <div class='headCont'>\n\
                    <h4 id='name'>\n\
                        <a href='http://www.facebook.com/" + fromId + "' target='_blank'>\n\
                            " + from + "         \n\
                        </a>\n\
                    " + type + " \n\
                    <a href='http://www.facebook.com/" + toId + "' target='_blank'>\n\
                            " + to + "         \n\
                        </a>\n\</h4>\n\
                </div>                \n\
            </div>            \n\
            <div class='message'>\n\
                " + message + "            \n\
            </div>\n\
            <div class='imagelnk'>\n\
                <a href='" + imgLink + "' target='_blank'>\n\
                    <img alt='Uploaded Image' src='" + imgSrc + "' />\n\
                </a>\n\
            </div>\n\
            <div id='options' class='options_" + msgId + "'>\n\
                <div onclick='likeComment(\"" + msgId + "\")' id='buttonlike_" + msgId + "' class='" + (userLikedPost ? "buttonliked" : "buttonunlike") + "'>\n\
                    <img id='like_" + msgId + "' src='img/" + (userLikedPost ? "likeok.png" : "like.png") + "'/>\n\
                </div>\n\
                <div class='buttoncomment' onclick='showCommentBox(\"" + msgId + "\")' id='buttoncomment_" + msgId + "'>\n\
                    <img src='img/comment.png'/>\n\
                </div>\n\
            </div>").hide().appendTo("#container").fadeIn('slow'); //.masonry( 'reload' );
    document.getElementById(msgId).date = date;
}

function createFbMediaAdd(fromId, from, message, imgSrc, imgLink, msgId, date, type, userLikedPost)
{

    $("<div id='" + msgId + "' onclick=\"showButtons('" + msgId + "')\" class='entryfb entry' data='" + date + "'>\n\
            <div class='head'>\n\
                <div>\n\
                    <a href='http://www.facebook.com/" + fromId + "' target='_blank'>\n\
                        <img class='fbimg' alt='Profile Image' src='https://graph.facebook.com/" + fromId + "/picture?type=square'/>\n\
                    </a>\n\
                </div>\n\
                <div class='headCont'>\n\
                    <h4 id='name'>\n\
                        <a href='http://www.facebook.com/" + fromId + "' target='_blank'>\n\
                            " + from + "         \n\
                        </a>\n\
                    " + type + "</h4>\n\
                </div>                \n\
            </div>            \n\
            <div class='message'>\n\
                " + message + "            \n\
            </div>\n\
            <div class='imagelnk'>\n\
                <a href='" + imgLink + "' target='_blank'>\n\
                    <img alt='Uploaded Image' src='" + imgSrc + "' />\n\
                </a>\n\
            </div>\n\
            <div id='options' class='options_" + msgId + "'>\n\
                <div onclick='likeComment(\"" + msgId + "\")' id='buttonlike_" + msgId + "' class='" + (userLikedPost ? "buttonliked" : "buttonunlike") + "'>\n\
                    <img id='like_" + msgId + "' src='img/" + (userLikedPost ? "likeok.png" : "like.png") + "'/>\n\
                </div>\n\
                <div class='buttoncomment' onclick='showCommentBox(\"" + msgId + "\")' id='buttoncomment_" + msgId + "'>\n\
                    <img src='img/comment.png'/>\n\
                </div>\n\
            </div>").hide().appendTo("#container").fadeIn('slow'); //.masonry( 'reload' );
    document.getElementById(msgId).date = date;
}

function createFbWallPost(fromId, toId, from, to, message, msgId, date, userLikedPost)
{
    $("<div id='" + msgId + "' onclick=\"showButtons('" + msgId + "')\" class='entryfb entry' data='" + date + "'>\n\
            <div class='head'>\n\
                <div>\n\
                    <a href='http://www.facebook.com/" + fromId + "' target='_blank'>\n\
                        <img class='fbimg' alt='Profile Image' src='https://graph.facebook.com/" + fromId + "/picture?type=square'/>\n\
                    </a>\n\
                </div>\n\
                <div class='headCont'>\n\
                    <h4 id='from'>\n\
                        <a href='http://www.facebook.com/" + fromId + "' target='_blank'>\n\
                            " + from + "\n\
                        </a>\n\
                     posted to:\n\
                    </h4>\n\
                    <h4 id='to'>\n\
                        <a href='http://www.facebook.com/" + toId + "' target='_blank'>\n\
                            " + to + "         \n\
                        </a>\n\
                    </h4>\n\
                </div>                \n\
            </div>            \n\
            <div class='message'>\n\
                " + message + "            \n\
            </div>\n\
            <div id='options' class='options_" + msgId + "'>\n\
                <div onclick='likeComment(\"" + msgId + "\")' id='buttonlike_" + msgId + "' class='" + (userLikedPost ? "buttonliked" : "buttonunlike") + "'>\n\
                    <img id='like_" + msgId + "' src='img/" + (userLikedPost ? "likeok.png" : "like.png") + "'/>\n\
                </div>\n\
                <div class='buttoncomment' onclick='showCommentBox(\"" + msgId + "\")' id='buttoncomment_" + msgId + "'>\n\
                    <img src='img/comment.png'/>\n\
                </div>\n\
            </div>").hide().appendTo("#container").fadeIn('slow'); //.masonry( 'reload' );
    document.getElementById(msgId).date = date;
}

function createFbStatusElement(fromId, name, message, msgId, date, userLikedPost) {

    $("<div id='" + msgId + "' onclick=\"showButtons('" + msgId + "')\" class='entryfb entry' data='" + date + "'>\n\
            <div class='head'>\n\
                <div>\n\
                    <a href='http://www.facebook.com/" + fromId + "' target='_blank'>\n\
                        <img class='fbimg' alt='Profile Image' src='https://graph.facebook.com/" + fromId + "/picture?type=square'/>\n\
                    </a>\n\
                </div>\n\
                <div class='headCont'>\n\
                    <h4 id='name'>\n\
                        <a href='http://www.facebook.com/" + fromId + "' target='_blank'>\n\
                            " + name + "         \n\
                        </a>\n\
                    posted</h4>\n\
                </div>                \n\
            </div>            \n\
            <div class='message'>\n\
                " + message + "            \n\
            </div>\n\
            <div id='options' class='options_" + msgId + "'>\n\
                <div onclick='likeComment(\"" + msgId + "\")' id='buttonlike_" + msgId + "' class='" + (userLikedPost ? "buttonliked" : "buttonunlike") + "'>\n\
                    <img id='like_" + msgId + "' src='img/" + (userLikedPost ? "likeok.png" : "like.png") + "'/>\n\
                </div>\n\
                <div class='buttoncomment' onclick='showCommentBox(\"" + msgId + "\")' id='buttoncomment_" + msgId + "'>\n\
                    <img src='img/comment.png'/>\n\
                </div>\n\
            </div>").hide().appendTo("#container").fadeIn('slow'); //.masonry( 'reload' );
    document.getElementById(msgId).date = date;
}

function createTweetElement(id, userId, userImg, userName, message, time, retweeted, favourited) {
    $("<div id='" + id + "' onclick=\"showButtons('" + id + "')\" class='entrytw entry'>\n\
            <div class='headtw head'>\n\
                <div>\n\
                    <a href='http://www.twitter.com/" + userId + "' target='_blank'>\n\
                        <img class='fbimg' alt='Profile Image' src='" + userImg + "'/>\n\
                    </a>\n\
                </div>\n\
                <div class='headCont'>\n\
                    <h4 id='name'>\n\
                         <a href='http://www.twitter.com/" + userId + "' target='_blank'>\n\
                    " + userName + " \n\
                    </a> posted </h4>\n\
                </div>                \n\
            </div>            \n\
            <div class='message'>\n\
                " + message + "           \n\
            </div> \n\
            <div id='options' class='options_" + id + "'>\n\
                <div onclick='retweetComment(\"" + id + "\",\"" + (retweeted ? "destory" : "retweet") + "\" )' id='buttonrt_" + id + "' class='" + (retweeted ? "buttonunrt" : "buttonrt") + "' >\n\
                    <img id='rt_" + id + "' src='img/" + (retweeted ? "retweeted.png" : "retweet.png") + "'/>\n\
                </div>\n\
                <div class='" + (favourited ? "buttonunfav" : "buttonfav") + "' onclick='favouriteComment(\"" + id + "\",\"" + (favourited ? "destory" : "retweet") + "\" )' id='buttoncomment_" + id + "'>\n\
                    <img src='img/favourite.png'/>\n\
                </div>\n\
            </div>\n\
            </div>").hide().appendTo("#container").fadeIn('slow'); //.masonry( 'reload' );
    document.getElementById(id).date = time;
}


/**
 * Closes the initial social network connectors overlay box
 */
function closeconnbox() {

    $(".networklink").fadeOut(500, function() {
        $("html").removeAttr('style');
        $("body").removeAttr('style');
        $(".slide").removeAttr('style');
        $(".content").css("display", "block");
        $("#container").css("z-index", "99999999999999999999");
        console.log(receiveddata);
        if (fbLogin && twLogin) {

            $("#container").html("<div class='loader'><img src='img/ajax-loader.gif'/><br/>Your data is currently being loaded...</div>")
            populateFacebookData('FacebookPull');
            populateTwitterData('TwitterGet');
        } else if (fbLogin && !twLogin) {

            $("#container").html("<div class='loader'><img src='img/ajax-loader.gif'/><br/>Your data is currently being loaded...</div>")
            populateFacebookData('FacebookPull');
        } //Maybe add another check for twitter token? But we probably don't need it here
        else {
            $("#container").html("<div class='loader'><img src='img/ajax-loader.gif'/><br/>Your data is currently being loaded...</div>")
            populateTwitterData('TwitterGet');
        }
    });



}


function retweetComment(id, meth) {
    $.ajax({
        type: 'POST',
        url: 'TwitterRT',
        data: {
            twId: id,
            method: meth
        }
    })

            .done(function(result) {
        console.log(result);
    });
}

function favouriteComment(id, meth) {
    $.ajax({
        type: 'POST',
        url: 'TwitterFav',
        data: {
            twId: id,
            method: meth
        }
    })

            .done(function(result) {
        console.log(result);
    });
}
/**
 * 
 Logs into Facebook. FB.login provides the popup dialogs necessary to authenticate
 the user and to allow user to accept permissions if necessary.*/
function FB_login() {

    FB.login(function(response) {
        if (response.authResponse) {

            FB.api('/me', function(response) {
                $('.headerbar').append("      - Welcome: " + response.name);


                FB.getLoginStatus(function(response) {


                    if (response.status === 'connected') {
                        console.log(response);
                        var uid = response.authResponse.userID;
                        var accessToken = response.authResponse.accessToken;
                        console.log("Data:\n" + uid + "   accesstoken: " + accessToken);

                        //Now store the access token in db
                        //Do AJAX POST and if return true, tick facebook box
                        $.ajax({
                            type: 'POST',
                            url: 'FacebookAuth',
                            data: {
                                fb_userid: uid,
                                fb_token: accessToken
                            }
                        })

                                //When completed
                                .done(function(returnData) {
                            if (returnData == "OK") {
                                $("#fbloginimg").attr("src", "img/facebooklogo-auth.png");
                            } else {
                                alert(returnData);
                            }

                        })

                                //If failed, alert reason and error codes
                                .fail(function(a, status, err) {
                            alert("status: " + status + "\nerror: " + err);
                        });
                    }
                });

            });
        } else {
            console.log('User cancelled login or did not fully authorize.');
        }
    }, {
        scope: 'read_stream, publish_stream'
    });
    fbLogin = true;
}






function likeComment(msgid) {

    FB.api(msgid + "/likes", "get", function(response) {
        var previouslyLiked = false;
        for (i in response.data) {

            if (response.data[i].id === facebookid) {
                previouslyLiked = true;
            }
        }

        var postid = msgid.split("_");
        var id = postid[0];

        if (!previouslyLiked) {


            FB.api("/" + msgid + "/likes", "post", function(response) {

                if (response === true) {

                    $("#like_" + msgid).attr('src', "img/likeok.png");
                    $("#buttonlike_" + msgid).css("background-color", "#D1F0D4");
                } else {
                    alert(JSON.stringify(response));
                }
            });
        } else {
            FB.api("/" + msgid + "/likes", "delete", function(response) {

                if (response === true) {

                    $("#like_" + msgid).attr('src', "img/like.png");
                    $("#buttonlike_" + msgid).css("background-color", "#EEE7F0");
                } else {
                    alert(JSON.stringify(response));
                }
            });
        }




    });
    $(".options_" + msgid).click(function(event) {
        event.stopPropagation();
    });


}

function logout() {
    if (confirm("Are you sure you want to logout?")) {
        window.location = "Logout";
    }
}

function deleteAcc() {
    if (confirm("Are you sure you want to delete your account?")) {
        $.post("RemoveAccount", null, function() {
            alert("Your account has been permanently deleted.");
            window.location = "index.htm";
        });
    }
}
