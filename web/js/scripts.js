var passwordreset = false;
var TsCsVisible = false;

/**
 * Expands the login area and replaces its contents with the signup form
 */
function signup() {
    $(".loginbox").animate({
        height: '+=260px'
    },
    
    //speed
    500,
    
    //method when complete
    function() {
        $(".loginform").load('./pages/signup.html', function(responseText) {
            $(".loginform").replaceWith(responseText);
        });
    });
}

/**
 * Shrinks the login area and replaces its contents with a login form
 */
function login() {
    $(".loginbox").animate({
        height: (passwordreset ? '+=80px' : '-=260px')
    },
    
    //speed
    500,
    
    //method when complete
    function() {
        $(".signupform").replaceWith("<form class='loginform' onsubmit='return submitLogin()'><table border='0' cellpadding='5px' class='logintable'><tr><td colspan='2'><h3>Login</h3></td></tr><tr><td>Username:</td><td><input type='text' id='txtUsername' name='txtUsername' placeholder='Username or email address' size='30' required></td></tr><tr><td>Password</td><td><input type='password' id='pwdPassword' name='pwdPassword' placeholder='Password' size='30' required></td></tr><tr><td>&nbsp;</td><td><input type='submit' value='Log In'></td></tr><tr class='smalltext'><td>&nbsp;</td><td><div onclick='signup()'>Sign up now!</div></td></tr></table></form> ");
    });

    passwordreset = false;
}

/**
 * Used to submit the Sign Up form over AJAX, validating data entry (that HTML5
 *  doesn't already do.
 * @returns {Boolean}
 *  Always returns false so that the browser doesn't reload the page. This
 *  function directs when all input was valid.
 */
function submitRegister() {
    var firstName = $("#txtFirstName").val();
    var surname = $("#txtSurname").val();
    var username = $("#txtUsername").val();
    var password = $("#pwdPassword").val();
    var confirm = $("#pwdConfirm").val();
    var chkTsCs = $("#chkTsCs").is(':checked');
    // Pop all the inputs into an array so we can loop through them later.
    var inputs = {
        "txtFirstName": 1,
        "txtSurname": 2,
        "txtUsername": 3,
        "pwdPassword": 4,
        "pwdConfirm": 5,
        "chkTsCs": 6
    };
    // If the passwords don't match, show the error
    if (password !== confirm) {
        document.getElementById('pwdPassword').setCustomValidity('Passwords must match.');
    } else {
        // Otherwise, everything was validated in HTML5 so lets continue
        document.getElementById('pwdPassword').setCustomValidity('');
        // Disable all the fields why we process
        for (var inputField in inputs) {
            $("#" + inputField).prop('disabled', true);
        }
        // Post the data
        $.post('Register', {
            firstName: firstName,
            surname: surname,
            username: username,
            password: password,
            confirm: confirm,
            chkTsCs: chkTsCs,
            dataOnly: "Y"
        },
        function(data) {
            // If we didn't get a success back...
            if (data.substring(0, "SUCCESS:".length) !== "SUCCESS:") {
                // Alert the error
                alert(data);
                // Re-enable all the fields
                for (inputField in inputs) {
                    $("#" + inputField).prop('disabled', false);
                }
                // Focus on the username field as this is what the error will be
                $("#txtUsername").focus();
            } else {
                // It all worked so direct to the page we're given
                window.location.href = data.substring("SUCCESS:".length);
            }
        });
    }
    return false;
}

/**
 * Used to submit the Login form over AJAX
 * @returns {Boolean}
 *  Always returns false so that the browser doesn't reload the page. This
 *  function directs when all input was valid.
 */
function submitLogin() {
    var username = $("#txtUsername").val();
    var password = $("#pwdPassword").val();

    // Pop all the inputs into an array so we can loop through them later.
    var inputs = {
        "txtUsername": 1,
        "pwdPassword": 2
    };

    // Disable all the fields why we process
    for (var inputField in inputs) {
        $("#" + inputField).prop('disabled', true);
    }

    // Post the data
    $.post('Login', {
        username: username,
        password: password,
        dataOnly: "Y"
    },
    function(data) {
        // If we didn't get a success back...
        if (data.substring(0, "SUCCESS:".length) !== "SUCCESS:") {
            // Alert the error
            alert(data);
            // Re-enable all the fields
            for (inputField in inputs) {
                $("#" + inputField).prop('disabled', false);
            }
            // Focus on the username field as this is what the error will be
            $("#txtUsername").focus();
        } else {
            // It all worked so direct to the page we're given
            window.location.href = data.substring("SUCCESS:".length);
        }
    });
    return false;
}

/**
 * Toggles display of the terms and conditions box
 */
function showHideTsCs() {
    $(".TsCs").animate({
        height: (TsCsVisible ? '-' : '+') + '=100px'
    }, 500);
    $(".loginbox").animate({
        overflow: 'y-scroll',
        height: (TsCsVisible ? '-' : '+') + '=100px'
    }, 500);
    TsCsVisible = !TsCsVisible;
}